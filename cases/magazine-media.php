<?php
$page = 'magazine-media';
include '../includes/header.php';
?>
<main class="c-site-content">
    <article class="o-section u-padding-top--s c-post c-post--portfolio">
        <header class="container">
            <div class="row u-margin-y--auto">
                <div class="col col-12 u-m-top--negative ">
                    <h4>Case  -  Magazine media</h4>
                    <div class="row u-margin-bottom--xl">
                        <div class="col col-12 col-lg-6">
                            <h1>Een inspiratieplatform voor de do's en don'ts in magazine advertising.</h1>
                        </div>
                        <div class="col col-12 col-lg-6 c-post__tagline">
                            <h2><span class="d-block u-padding-left--xxl">adverteerders</span>
                                <span class="d-block">en redacties</span>
                                <span class="d-block u-padding-left--xl">connecteren</span>
                            </h2>
                        </div>
                        <div class="col col-12">
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="c-post__body">
            <img src="/images/cases/magazinemedia/Artboard-2-copy-6@2x-min.jpg" alt="test image" class="img-fluid"/>
            <div class="container">
                <div class="row">
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/magazinemedia/_DSC4172-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/magazinemedia/3_DSC4149-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/magazinemedia/6_DSC4271-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/magazinemedia/7_DSC4191-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/magazinemedia/5_STRAK-portfolio-beeld-MMBIGWRAPS005-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/magazinemedia/6_STRAK-portfolio-beeld-MMBIGWRAPS001-min.jpg"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/magazinemedia/Artboard-2-copy-5@2x-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
            </div>

           <!-- <div class="u-fullwidth--image" data-aos="fade-in">
                <div class="c-background-image js-lazyload" data-src="http://via.placeholder.com/1920x1000"></div>
            </div>-->
        </div>
    </article>
    <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>