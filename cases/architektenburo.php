<?php
$page = 'architektenburo';
include '../includes/header.php';
?>
<main class="c-site-content">
    <article class="o-section u-padding-top--s c-post c-post--portfolio">
        <header class="container">
            <div class="row u-margin-y--auto">
                <div class="col col-12 u-m-top--negative ">
                    <h4>Case  -  Architektenburo</h4>
                    <div class="row u-margin-bottom--xxl">
                        <div class="col col-12 col-lg-6">
                            <h1>Door dingen weg te laten de essentie van architectuur te laten spreken.</h1>
                        </div>
                        <div class="col col-12 col-lg-6 c-post__tagline">
                            <h2><span class="d-block u-padding-left--xxl">ruimte,</span>
                                <span class="d-block">licht en orde</span>
                                <span class="d-block u-padding-left--xl">als identiteit</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="c-post__body">
            <div class="container">
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/architektenburo/Architektenburo-BusinessCardsMockup-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
                <div class="row justify-content-center c-post__testimonial">
                    <div class="col col-12 col-md-10">
                        <p>Architecture is the learned game, correct and magnificent,<br> of forms assembled in the light.</p>
                        <p class="author">Le Corbusier</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-10" data-aos="fade-in">
                        <img src="/images/cases/architektenburo/STRAK-Facebook-Architektenburo_2-08.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
            </div>
            <div class="container-fluid u-padding-left--flush u-padding-right--flush">
                        <img src="/images/cases/architektenburo/screencapture-architektenburo-be-01-min.jpg" alt="test image" class="img-fluid"/>
            </div>
            <div class="container">
                <div class="row u-margin-bottom--flush">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/architektenburo/architektenburo-be-02-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/architektenburo/untitled-7280-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
            </div>

           <!-- <div class="u-fullwidth--image" data-aos="fade-in">
                <div class="c-background-image js-lazyload" data-src="http://via.placeholder.com/1920x1000"></div>
            </div>-->
        </div>
    </article>
    <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>