<?php
$page = 'techorama';
include '../includes/header.php';
?>
<main class="c-site-content">
    <article class="o-section u-padding-top--s c-post c-post--portfolio">
        <header class="container">
            <div class="row u-margin-y--auto">
                <div class="col col-12 u-m-top--negative">
                    <h4>Case  -  Techorama</h4>
                    <div class="row u-margin-bottom--xl">
                        <div class="col col-12 col-lg-6">
                            <h1>Een IT-Conference in <br>een uniek vintage jasje.</h1>
                        </div>
                        <div class="col col-12 col-lg-6 c-post__tagline">
                            <h2><span class="d-block u-padding-left--xxl">Het kind in de</span>
                                <span class="d-block">IT professional</span>
                                <span class="d-block u-padding-left--xl">aanspreken</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="c-post__body">
            <div class="container">
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/techorama/TECHORAMA-brand001-grijs-min.jpg" alt="techorama" class="img-fluid"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/techorama/34790411442_00dc607f96_o-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/techorama/_DSC2757-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/techorama/_DSC2910-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/techorama/_DSC2888-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/techorama/STRAK-CASE-TECHORAMA-001-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/techorama/_DSC2797-min.jpg"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/techorama/STRAK-CASE-TECHORAMA-varia-min.jpg" alt="techorama" class="img-fluid"/>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-8" data-aos="fade-in">
                        <img src="/images/cases/techorama/_DSC2824-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/techorama/STRAK-CASE-TECHORAMA-socialposts-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                    <div class="col col-12 text-center">
                        <a href="https://techorama.be/" title="Techorama" class="c-btn c-btn--gradient u-margin-left--auto u-margin-right--auto">Bezoek website</a>
                    </div>
                </div>
            </div>

           <!-- <div class="u-fullwidth--image" data-aos="fade-in">
                <div class="c-background-image js-lazyload" data-src="http://via.placeholder.com/1920x1000"></div>
            </div>-->
        </div>
    </article>
    <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>