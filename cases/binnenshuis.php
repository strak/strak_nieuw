<?php
$page = 'binnenshuis';
include '../includes/header.php';
?>
<main class="c-site-content">
    <article class="o-section u-padding-top--s c-post c-post--portfolio">
        <header class="container">
            <div class="row u-margin-y--auto">
                <div class="col col-12 u-m-top--negative">
                    <h4>Case  -  Binnenshuis</h4>
                    <div class="row u-margin-bottom--xxl">
                        <div class="col col-12 col-lg-6">
                            <h1>Niemand woont zoals jij.<br>Binnenshuis zorgt voor sfeer op maat van uw interieur.</h1>
                        </div>
                        <div class="col col-12 col-lg-6 c-post__tagline">
                            <h2><span class="d-block u-padding-left--xxl">gezelligheid,</span>
                                <span class="d-block">persoonlijkheid</span>
                                <span class="d-block u-padding-left--xl">& esthetiek</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="c-post__body">
            <div class="container">
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/binnenshuis/binnenshuis1.jpg" alt="binnenshuis" class="img-fluid"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/binnenshuis/binnenshuis5.jpg" alt="binnenshuis grafisch ontwerp" class="img-fluid"/>
                    </div>
                </div>
            </div>
            <div class="o-section u-margin-bottom--xxl">
                <img src="/images/cases/binnenshuis/binnenshuis3.jpg" alt="binnenshuis grafisch ontwerp" class="img-fluid"/>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/binnenshuis/binnenshuis2.jpg" alt="binnenshuis2" class="img-fluid"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12 col-md-4" data-aos="fade-in">
                        <div class="c-post__image c-image--small">
                            <div class="c-background-image js-lazyload" data-src="/images/cases/binnenshuis/binnenshuis6.jpg"></div>
                        </div>
                    </div>
                    <div class="col col-12 col-md-4" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-post__image c-image--small">
                            <div class="c-background-image js-lazyload" data-src="/images/cases/binnenshuis/binnenshuis7.jpg"></div>
                        </div>
                    </div>
                    <div class="col col-12 col-md-4" data-aos="fade-in">
                        <div class="c-post__image c-image--small">
                            <div class="c-background-image js-lazyload" data-src="/images/cases/binnenshuis/binnenshuis8.jpg"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>