<?php
$page = 'klarafestival';
include '../includes/header.php';
?>
<main class="c-site-content">
    <article class="o-section u-padding-top--s c-post c-post--portfolio">
        <header class="container">
            <div class="row u-margin-y--auto">
                <div class="col col-12 u-m-top--negative">
                    <h4>Case  -  Klarafestival</h4>
                    <div class="row">
                        <div class="col col-12 col-lg-6">
                            <h1>Vorm en functie perfect op elkaar afgestemd. <br> Webdesign die feilloos met de inhoud mee-evolueert.</h1>
                        </div>
                        <div class="col col-12 col-lg-6 c-post__tagline">
                            <h2><span class="d-block u-padding-left--xl">emotie</span>
                                <span class="d-block">die je hoort,</span>
                                <span class="d-block u-padding-left--xxl">laten spreken</span>
                            </h2>
                        </div>
                    </div>
                    <p>Zoals voor elk festival is het programma de drijvende kracht om tickets te verkopen. Hiervoor ontwierpen we een design stramien waar zowel statische als video content perfect tot z'n recht komt.</p>
                </div>
            </div>
        </header>
        <div class="c-post__body">
            <div class="container-fluid u-padding-left--flush u-padding-right--flush">
                        <img src="/images/cases/klarafestival/Artboard-2-copy-5@2x-min.jpg" alt="klarafestival" class="img-fluid"/>
                        <img src="/images/cases/klarafestival/Group-1359-min.jpg" alt="klarafestival" class="img-fluid"/>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col col-12 col-md-10">
                        <p>
                        Naast de website stonden wij eveneens in voor het ontwerp en de productie van het jaarverslag van Festival van Vlaanderen Brussel, de organisator van het Klarafestival. Hier bespeelden we de zintuigen door gebruik te maken van extra tactiele papiersoorten in combinatie met hoogstaande afwerkingstechnieken.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/klarafestival/klara-7238-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/klarafestival/klara-7191-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/klarafestival/_DSC8781-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/klarafestival/_DSC8818-min.jpg"></div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-10" data-aos="fade-in">
                        <img src="/images/cases/klarafestival/_DSC8846-min.jpg" alt="klarafestival" class="img-fluid"/>
                    </div>
                </div>
            </div>

           <!-- <div class="u-fullwidth--image" data-aos="fade-in">
                <div class="c-background-image js-lazyload" data-src="http://via.placeholder.com/1920x1000"></div>
            </div>-->
        </div>
    </article>
    <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>