<?php
$page = 'cases';
include '../includes/header.php';

?>
<main class="c-site-content">
    <section class="o-section u-padding-top--s">
        <div class="container">
            <div class="row">
                <div class="col col-12 col-md-12 u-m-top--negative">
                    <h4>Cases</h4>
                    <h1>Onze honger en ons vakmanschap drijft ons om telkens zorgvuldig doordachte concepten te ontwikkelen die helder, flexibel en schaalbaar zijn.</h1>
                    <p>We zijn trots op wat we maken en waar je trots op bent moet je laten zien. Nietwaar? Neem gerust een kijkje naar onderstaande selectie van verwezenlijkingen. Laat je inspireren door onze verhalen, belevenissen en resultaten. Van branding over website tot conversie.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row no-gutters u-margin-top--xl">
                <div class="col col-12 col-md-6">
                    <article class="c-card--teaser" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/binnenshuis.jpg"></div>
                        <div class="c-card__body">
                            <h3><span class="d-block u-padding-left--xxl">gezelligheid,</span>
                                <span class="d-block">persoonlijkheid</span>
                                <span class="d-block u-padding-left--xl">& esthetiek</span>
                            </h3>
                            <a href="/cases/binnenshuis" title="binnenshuis">Binnenshuis</a>
                        </div>
                    </article>
                </div>
                <div class="col col-12 col-md-6">
                    <article class="c-card--teaser" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/architectenburo.jpg"></div>
                        <div class="c-card__body">
                            <h3><span class="d-block u-padding-left--xl">ruimte,</span>
                                <span class="d-block">licht en orde</span>
                                <span class="d-block u-padding-left--xl">als identiteit</span>
                            </h3>
                            <a href="/cases/architektenburo" title="Architektenburo">Architektenburo</a>
                        </div>
                    </article>
                </div>
                <div class="col col-12">
                    <article class="u c-card--teaser c-card--big" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk.jpg"></div>
                        <div class="c-card__body">
                            <h3><span class="d-block u-padding-left--xl">de essentie</span>
                                <span class="d-block">van een bedrijf</span>
                                <span class="d-block u-padding-left--xxl">als merknaam</span>
                            </h3>
                            <a href="/cases/sportwerk" title="sportwerk vlaanderen">Sportwerk</a>
                        </div>
                    </article>
                </div>
                <div class="col col-12 col-md-6">
                    <article class="c-card--teaser" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/techorama.jpg"></div>
                        <div class="c-card__body">
                            <h3><span class="d-block u-padding-left--xxl">het kind in de</span>
                                <span class="d-block">IT professional</span>
                                <span class="d-block u-padding-left--xl">aanspreken</span>
                            </h3>
                            <a href="/cases/techorama" title="techorama">Techorama</a>
                        </div>
                    </article>
                </div>
                <div class="col col-12 col-md-6">
                    <article class="c-card--teaser" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/magazinemedia.jpg"></div>
                        <div class="c-card__body">
                            <h3><span class="d-block u-padding-left--xxl">adverteerders</span>
                                <span class="d-block">en redacties</span>
                                <span class="d-block u-padding-left--xl">connecteren</span>
                            </h3>
                            <a href="/cases/magazine-media" title="Magazine media">Magazine media</a>
                        </div>
                    </article>
                </div>
                <div class="col col-12">
                    <article class="u c-card--teaser c-card--big" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/klarafestival.jpg"></div>
                        <div class="c-card__body">
                            <h3><span class="d-block u-padding-left--xl">emotie</span>
                                <span class="d-block">die je hoort,</span>
                                <span class="d-block u-padding-left--xxl">laten spreken</span>
                            </h3>
                            <a href="/cases/klarafestival" title="sportwerk vlaanderen">Klarafestival</a>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <?php include '../includes/cta.php'; ?>
</main>
    <?php include '../includes/footer.php'; ?>