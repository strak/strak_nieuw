<?php
$page = 'sportwerk';
include '../includes/header.php';
?>
<main class="c-site-content">
    <article class="o-section u-padding-top--s c-post c-post--portfolio">
        <header class="container">
            <div class="row u-margin-y--auto">
                <div class="col col-12 u-m-top--negative u-margin-bottom--xl">
                    <h4>Case  -  Sportwerk</h4>
                    <div class="row">
                        <div class="col col-12 col-lg-6">
                            <h1>Duidelijker en menselijker vertellen waar
                                een bedrijf en z’n mensen voor staan.</h1>
                        </div>
                        <div class="col col-12 col-lg-6 c-post__tagline">
                            <h2><span class="d-block u-padding-left--xxl">de essentie</span>
                                <span class="d-block">van een bedrijf</span>
                                <span class="d-block u-padding-left--xl">als merknaam</span>
                            </h2>
                        </div>
                    </div>
                </div>


            </div>
        </header>
        <div class="c-post__body">
            <div class="container">
                <div class="row">
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/_DSC2601-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/_DSC2589-min.jpg"></div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-10">
                        <h2>De synergie van naam en symboliek</h2>
                        <p>De oude naam had binnen zijn doelgroep niet veel waarde opgebouwd en liet bij nieuwkomers geen impressie na. Door de taak, boodschap van het bedrijf te benoemen kwamen we uit op de nieuwe naam en uiteindelijk ook baseline. SPORTWERK Vlaanderen, maakt werk van sport.</p>
                        <p>Er resteerde ons hier enkel nog een werkbaar concept aan te koppelen. Naast de naam gaan we ook op zoek naar een symboliek die het concept kan dragen. Het zou meegenomen zijn als we de sportlesgevers hierbij hun trotsheid konden stimuleren en ontwierpen hiervoor een embleem binnen de huisstijl die later eveneens deel van het logo werd.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/_DSC2570-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/_DSC2695OK-min.jpg"></div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-10 video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/T112pBUs4S0?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/sportwerk/STRAK-CASE-SPORTWERK-identity-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/_DSC2661-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/untitled-6578-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/untitled-6061-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in" data-aos-delay="200">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/_DSC2613-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/untitled-6400-min.jpg"></div>
                    </div>
                    <div class="col col-12 col-md-6 c-post__image" data-aos="fade-in">
                        <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk/untitled-6172-min.jpg"></div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-10" data-aos="fade-in">
                        <img src="/images/cases/sportwerk/STRAK-CASE-SPORTWERK-wwwbruisen2-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col col-12 col-md-10 video-container">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/LXx1fxp26yk?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/sportwerk/STRAK-CASE-SPORTWERK-infographic-min.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12 col-lg-6">
                        <div class="video-container video-container--small">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/M3sm-xSf56A?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col col-12 col-lg-6">
                        <div class="video-container video-container--small">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/-feyF8c-L2Q?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-12" data-aos="fade-in">
                        <img src="/images/cases/sportwerk/untitled-6722-kleuren.jpg" alt="test image" class="img-fluid"/>
                    </div>
                </div>
            </div>

           <!-- <div class="u-fullwidth--image" data-aos="fade-in">
                <div class="c-background-image js-lazyload" data-src="http://via.placeholder.com/1920x1000"></div>
            </div>-->
        </div>
    </article>
    <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>