<?php
$page = 'bedankt';

include 'includes/header.php';
?>
<main class="c-site-content">
    <div class="o-section  u-padding-top--s">
        <div class="container">
            <div class="row">
                <div class="col col-12 u-m-top--negative">
                    <h4>Contact</h4>
                    <h1>Strak plan om ons te mailen!</h1>
                    <p>Wij nemen zo snel mogelijk contact op met jou</p>

                    <p>Tot hoors!</p>
                    <p>Bekijk alvast ook onze <a href="/cases" title="cases">cases</a></p>
                </div>

            </div>
        </div>
    </div>
    <?php
    include 'includes/cta.php';
    ?>
</main>

<?php
include 'includes/footer.php';
?>
