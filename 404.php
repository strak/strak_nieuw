<?php include 'includes/header.php'?>

    <main class="c-site-content">
        <section class="o-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        Whoops, dat ging minder Strak dan gepland.
                    </div>
                    <div class="col-6">
                        <p>
                            Gebruik alvast een van onderstaande links om op je bestemming te geraken.
                        </p>
                        <ul>
                            <li><a href="/aanpak" title="aanpak">Aanpak</a></li>
                            <li><a href="/diensten" title="diensten">Diensten</a></li>
                            <li><a href="/cases" title="cases">Cases</a></li>
                            <li><a href="/jobs" title="jobs">Jobs</a></li>
                            <li><a href="/contact" title="contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/cta.php'?>
    </main>

<?php include 'includes/footer.php'?>