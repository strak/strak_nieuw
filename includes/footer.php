
<div class="footer__spacer">
</div>

<footer class="c-site-footer">
    <div class="c-footer__content">
        <div class="container">
            <div class="row c-site-footer--primary">
                <div class="col col-12 col-sm-8">
                    <div class="row">
                        <div class="col col-12 col-sm-6 col-md-5">
                            <div class="textwidget">
                                <p>Evergemsesteenweg 195<br>
                                    9032 Gent, Belgi&#235;</p>
                                <p><a href="tel:+32093352274" title="tel">+32 (0) 9 335 22 74</a></p>
                                <p><a href='mailto&#58;pla%6&#69;%40st&#114;ak&#46;%&#54;2e' title="mail ons">plan&#64;strak&#46;b&#101;</a></p>
                                <p>BTW:&nbsp;BE 0627 793 007</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12 col-sm-4 lead">
                    <div class="textwidget"><p>We zijn constant op zoek naar nieuwe talenten. Denk jij dat je hebt wat nodig is? Op dit moment zoeken we:</p>
                        <ul class="c-list c-list--asterisk c-list--footer">
                            <li><a href="/jobs/senior-developer">Senior PHP developer</a> </li>
                            <li><a href="/jobs/junior-developer">Junior PHP developer</a> </li>
                            <li><a href="/jobs/junior-online-marketeer">Junior online marketeer</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row c-site-footer--secondary" role="contentinfo">
                <div class="col col-12 col-sm-6 col-md-6">
                    <ul id="legal-menu" class="c-navigation c-navigation--legal"><li id="menu-item-341" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-341"><a href="/voorwaarden" title="algemene voorwaarden">Algemene voorwaarden</a></li>
                        <li id="menu-item-353" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-353"><a href="/cookies" title="privacy">Cookie policy</a></li>
                        <li id="menu-item-352" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-352"><a href="/disclaimer" title="disclaimer">Disclaimer</a></li>
                    </ul>		</div>
                <div class="col col-12 col-sm-6 col-md-6">
                    <ul class="o-list c-social">
                        <li>
                            <a href="https://www.facebook.com/strak.be/" title="Facebook" target="_blank">
                                <svg class="c-icon c-icon--facebook">
                                    <use xlink:href="#facebook"></use>
                                </svg>
                                <span class="screen-reader-text">Facebook</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://instagram.com/strak_be" title="Instagram" target="_blank">
                                <svg class="c-icon c-icon--instagram">
                                    <use xlink:href="#instagram"></use>
                                </svg>
                                <span class="screen-reader-text">Instagram</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/strak_be" title="Twitter" target="_blank">
                                <svg class="c-icon c-icon--twitter">
                                    <use xlink:href="#twitter"></use>
                                </svg>
                                <span class="screen-reader-text">Twitter</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117755928-1"></script>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-117755928-1');
</script>
<!-- Hotjar Tracking Code for https://www.strak.be/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:852729,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/vendor/blazy.min.js"></script>
<script src="/js/vendor/aos.min.js"></script>
<script src="/js/vendor/slick.min.js"></script>
<script src="/js/slick-ext.js"></script>
<script src="/js/main.js"></script>
<!--Start of Tawk.to Script-->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5ae02acf5f7cdf4f0533975c/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->

</body>
</html>