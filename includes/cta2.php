<section class="c-cta">
    <div class="container">
        <div class="row">
            <div class="col col-12 col-lg-8">
                <div class="row">
                    <div class="col-12 col-lg-7 u-text--center">
                            <h3>
                                <span class="d-block u-padding-left--xl">Bekijk alvast</span>
                                <span class="d-block">een greep uit</span>
                                <span class="d-block u-padding-left--xxl">ons werk</span>
                            </h3>
                        <a href="/cases" class="c-btn c-btn--gradient" title="Bekijk onze cases">Cases</a>
                    </div>
                </div>
            </div>
            <div class="col col-12 col-lg-4 ">
                <p class="c-cta__text">Nog wat overtuiging nodig?<br>Snuister gerust even door ons werk.<br>
                    <span class="u-margin-top--l">Of gaan we er samen even door?</span></p>
            </div>
        </div>
    </div>
</section>