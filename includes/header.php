<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- FB META -->
    <?php
    switch ($page) {
        case 'aanpak':
            ?>
            <meta property="og:url"                content="https://strak.be/aanpak" />
            <meta property="og:type"               content="website" />
            <meta property="og:title"              content="Strak - Aanpak" />
            <meta name="description" property="og:description"        content="Onze expertise stelt ons in staat om snel de juiste vragen te stellen en hiervoor concrete oplossingen aan te bieden. Creativity as a Service, zo werken we telkens samen naar een goede oplossing." />
            <meta property="og:image"              content="https://strak.be/images/STRAK-socialshare.png" />
            <?php
            break;
        case 'diensten':
        case 'strategy':
        case 'branding':
        case 'development':
        case 'marketing':
            ?>
            <meta property="og:url"                content="https://strak.be/diensten" />
            <meta property="og:type"               content="website" />
            <meta property="og:title"              content="Strak - Diensten op maat voor uw project" />
            <meta name="description" property="og:description"        content="Van strategie en concept over webdesign t.e.m. conversie. Wij gaan voor de volledige fit van A tot Z voor jouw bedrijf, merk of project." />
            <meta property="og:image"              content="https://strak.be/images/STRAK-socialshare.png" />
            <?php
            break;
        case 'cases':
            ?>
            <meta property="og:url"                content="https://strak.be/cases" />
            <meta property="og:type"               content="website" />
            <meta property="og:title"              content="Strak - Een greep uit ons werk" />
            <meta name="description" property="og:description"        content="Als creatief bureau verzorgden we reeds heel wat projecten. Strategie, Concept, Branding, Webdesign. Bekijk hier enkele van onze verwezenlijkingen." />
            <meta property="og:image"              content="https://strak.be/images/STRAK-socialshare.png" />
            <?php
            break;
        case 'jobs':
        case 'junior-developer':
        case 'junior-online-marketeer':
        case 'senior-developer':
            ?>
            <meta property="og:url"                content="https://strak.be/jobs" />
            <meta property="og:type"               content="website" />
            <meta property="og:title"              content="Strak - Op zoek naar een uitdaging?" />
            <meta name="description" property="og:description"        content="Wil jij ons team van gepassioneerde ontwerpers, marketeers en developers versterken? Neem dan gerust een kijkje naar onze openstaande vacatures." />
            <meta property="og:image"              content="https://strak.be/images/STRAK-socialshare.png" />
            <?php
            break;
        case 'contact':
            ?>
            <meta property="og:url"                content="https://strak.be/contact" />
            <meta property="og:type"               content="website" />
            <meta property="og:title"              content="Strak - Kom gerust even langs" />
            <meta property="og:description"        content="Of het nu over een identiteit, campagne of website gaat, wij maken jouw merk zichtbaar, relevant en effectief. Wens je creatief te sparren? Daag ons uit!" />
            <meta property="og:image"              content="https://strak.be/images/STRAK-socialshare.png" />
            <?php
            break;
        case 'persmededeling-2018':
            ?>
            <meta property="og:url"                content="https://strak.be/persmededeling-2018/" />
            <meta property="og:type"               content="website" />
            <meta property="og:title"              content="Strak - Persmededeling 2018" />
            <meta name="description" property="og:description"        content="STRAK vanaf nu een one-stop shop voor creatieve communicatie" />
            <meta property="og:image"              content="https://strak.be/images/STRAK-socialposterbis3.gif" />
            <?php
            break;
        default:
            ?>
            <meta property="og:url"                content="https://strak.be" />
            <meta property="og:type"               content="website" />
            <meta property="og:title"              content="STRAK – Intelligente, creatieve oplossingen" />
            <meta name="description" property="og:description"        content="Bij Strak bouwen we merken en voeren de communicatie die je bijblijft. Van strategie en concept over webdesign tot en met conversie." />
            <meta property="og:image"              content="https://strak.be/images/STRAK-socialshare.png" />
            <?php
            break;
    }
    ?>

    <title>Strak - Creatief bureau</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-ext.css" rel="stylesheet">
    <link href="/css/vendor/aos.min.css" rel="stylesheet">
    <link href="/css/vendor/slick.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1348402448514959');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=1348402448514959&ev=PageView
&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    <style>
        .antispam {
            display: none;
        }
    </style>
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
            {'GTM-NRC4ZJ5':true});</script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-117755928-1', 'auto');
        ga('require', 'GTM-NRC4ZJ5');

    </script>
    <script type="application/ld+json">
    {
      "@context" : "http://schema.org",
      "@type" : "LocalBusiness",
      "name" : "STRAK",
      "image" : "https://strak.be/images/strak.svg",
      "telephone" : "+32 (0) 9 335 22 74",
      "email" : "plan@strak.be",
      "address" : {
        "@type" : "PostalAddress",
        "streetAddress" : "Evergemsesteenweg 195",
        "addressLocality" : "Gent",
        "addressCountry" : "België",
        "postalCode" : "9032"
      }
    }
    </script>
</head>
<body>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" style="display:none;">
    <symbol id="strak">
        <title>STRAK</title>
        <path d="M62.6,50.4c-13.7-3.4-19.2-5.8-21.1-7c-2.3-1.4-3.8-3.8-3.8-6.6c-0.1-2.6,1-5.2,3-6.9c2.7-1.9,6-2.9,9.3-2.7c5.3,0,10.7,1,15.7,2.9L44.2,0c-9.2,0.7-17.9,3.9-25.4,9.3c-8.6,6.6-13.1,16-13.1,28s3.5,21,10.4,26.7c6.7,5.4,17.3,10.1,31.5,13.4c8.5,2.1,14.2,4.2,16.9,6.1c3.8,2.8,4.7,8.2,1.8,12c-0.5,0.7-1,1.2-1.7,1.7c-3.1,2-6.7,2.9-10.4,2.7c-10.4,0-22-5.6-34.5-16.5l-1.6-1.4L0,105l1.4,1.3C16.9,120.6,34.7,128,54.1,128c13.6,0,24.8-3.5,33.1-10.6c8.3-6.8,13.1-17.1,12.8-27.8c0-11.4-3.4-20.2-10.1-26.1S74.3,53.3,62.6,50.4z"/>
    </symbol>
    <symbol id="facebook" viewBox="0 0 24 24">
        <title>Facebook</title>
        <path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"/>
    </symbol>
    <symbol id="twitter" viewBox="0 0 24 24">
        <title>Twitter</title>
        <path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"/>
    </symbol>
    <symbol id="instagram" viewBox="0 0 24 24">
        <title>Instagram</title>
        <path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z"/>
    </symbol>
    <symbol id="linkedin" viewBox="0 0 24 24">
        <title>LinkedIn</title>
        <path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z"/>
    </symbol>
    <symbol id="github" viewBox="0 0 24 24">
        <title>GitHub</title>
        <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z"/>
    </symbol>
    <symbol id="bitbucket" viewBox="0 0 24 24">
        <title>Bitbucket</title>
        <path d="M23.37,1.26H.78A.75.75,0,0,0,0,2H0v.15L3.27,21.92a1,1,0,0,0,.35.61h0a1.16,1.16,0,0,0,.62.22H20a.77.77,0,0,0,.77-.64L23,8.49h0l1-6.34a.77.77,0,0,0-.63-.89ZM14.52,15.54h-5l-1.34-7h7.56Z"/>
    </symbol>
    <symbol id="left" viewBox="0 0 20 20">
        <title>Left</title>
        <path d="M4.3,10c0,0.4,0.1,0.7,0.4,1l8.6,8.6c0.6,0.6,1.4,0.6,2,0c0.6-0.6,0.6-1.4,0-2L7.7,10l7.6-7.6c0.6-0.6,0.6-1.4,0-2c-0.6-0.6-1.4-0.6-2,0c0,0,0,0,0,0L4.7,9C4.4,9.3,4.3,9.6,4.3,10z"/>
    </symbol>
    <symbol id="right" viewBox="0 0 20 20">
        <title>Right</title>
        <path d="M15.7,10c0-0.4-0.1-0.7-0.4-1L6.7,0.4c-0.6-0.6-1.4-0.6-2,0s-0.6,1.4,0,2l7.6,7.6l-7.6,7.6c-0.6,0.6-0.6,1.4,0,2c0.6,0.6,1.4,0.6,2,0c0,0,0,0,0,0l8.6-8.6C15.6,10.7,15.7,10.4,15.7,10z"/>
    </symbol>
</svg>

<header class="c-site-header <?php if( $page == 'index') echo 'c-site-header--home'?>">

    <div class="container">
        <nav class="c-nav c-nav--primary" role="navigation" aria-label="Primary Navigation">
            <a href="/" title="WordPress" class="c-site-title" rel="home" aria-label="WordPress">
                <img src="/images/strak.svg" class="attachment-logo size-logo" alt="STRAK">
            </a>
            <ul id="header-menu" class="d-none d-lg-block c-navigation c-navigation--primary">
                <li <?php if( $page == 'aanpak') echo 'class="current_page_item"'?>><a href="/aanpak" title="aanpak">Aanpak</a></li>
                <li <?php if( $page == 'diensten') echo 'class="current_page_item"'?>><a href="/diensten" title="diensten">Diensten</a></li>
                <li <?php if( $page == 'cases') echo 'class="current_page_item"'?>><a href="/cases" title="cases">Cases</a></li>
                <li <?php if( $page == 'jobs') echo 'class="current_page_item"'?>><a href="/jobs" title="jobs">Jobs</a> <div class="c-navigation__count">3</div></li>
                <li <?php if( $page == 'contact') echo 'class="current_page_item"'?>><a href="/contact" title="contact">Contact</a></li>
            </ul>
            <input type="checkbox" id="navigation-toggle" class="c-navigation__checkbox is-hidden js-navigation-toggle" hidden="hidden">
            <label for="navigation-toggle" class="d-lg-none c-navigation__toggle">
                <i></i>
                <i></i>
                <i></i>
            </label>
            <div id="header-menu" class="d-lg-none c-navigation c-navigation--mobile">
                <ul>
                    <li <?php if( $page == 'index') echo 'class="current_page_item"'?>><a href="/index" title="home">Home</a></li>
                    <li <?php if( $page == 'aanpak') echo 'class="current_page_item"'?>><a href="/aanpak" title="aanpak">Aanpak</a></li>
                    <li <?php if( $page == 'diensten') echo 'class="current_page_item"'?>><a href="/diensten" title="diensten">Diensten</a></li>
                    <li <?php if( $page == 'cases') echo 'class="current_page_item"'?>><a href="/cases" title="cases">Cases</a></li>
                    <li <?php if( $page == 'jobs') echo 'class="current_page_item"'?>><a href="/jobs" title="diensten">Jobs</a></li>
                    <li <?php if( $page == 'contact') echo 'class="current_page_item"'?>><a href="/contact" title="contact">Contact</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <?php if( $page == 'index'){?>
        <div class="container">
            <div class="row  content-middle c-card--home ">
                <div class="col col-12 col-md-6 push-md-1 c-card__body" data-aos="fade-right">
                    <h1>STRAK, wij bouwen merken en voeren de communicatie die je bijblijft.</h1>
                    <div class="button-container-mobile"><a href="/contact" title="diensten" class="c-btn c-btn--gradient">Contacteer ons</a></div>
                </div>
                <div class="col col-12 col-md-6 pull-md-1 c-card__image" data-aos="fade-in">
                    <div class="c-background-image js-lazyload" data-src="/images/homepage/_DSC2720-min.png"></div>
                </div>
            </div>
        </div>
    <?php }?>
</header>