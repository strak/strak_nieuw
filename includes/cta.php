<section class="c-cta">
    <div class="container">
        <div class="row">
            <div class="col col-12 col-lg-8">
                <div class="row">
                    <div class="col-12 col-lg-7 u-text--center">
                        <?php if($page == 'branding') {  ?>
                        <h3>
                            <span class="d-block u-padding-left--xl">nood aan</span>
                            <span class="d-block">een nieuw</span>
                            <span class="d-block">logo</span>
                        </h3>
                        <?php } else { ?>
                            <h3>
                                <span class="d-block u-padding-left--xl">Vertel ons</span>
                                <span class="d-block">over jouw</span>
                                <span class="d-block u-padding-left--xxl">project</span>
                            </h3>
                        <?php } ?>
                        <a href="/contact" class="c-btn c-btn--gradient" title="contacteer ons">Contacteer ons</a>
                    </div>
                </div>
            </div>
            <div class="col col-12 col-lg-4 ">
                <p class="c-cta__text">
                <p class="c-cta__text">Of het nu over een identiteit,<br> campagne of website gaat, wij maken jouw merk<br>
                    <span class="u-margin-top--l">zichtbaar, relevant en effectief.</span></p>
            </div>
        </div>
    </div>
</section>