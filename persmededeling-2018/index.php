<?php
$page = 'persmededeling-2018';

include '../includes/header.php';
?>
    <main class="c-site-content">
    <section class="o-section u-padding-top--s">
                <div class="container">
                    <div class="row u-padding-bottom--l">
                        <div class="col col-12 u-m-top--negative">
                            <h4>Persmededeling</h4>
                            <h1>STRAK vanaf nu een one-stop shop <br>voor creatieve communicatie</h1>
                            <p>STRAK creatief bureau kreeg op haar verjaardag een uniek cadeau, de versterking van twee andere zusterbedrijven uit de <a href="https://mandelbrot.be/" target="_blank" title="mandelbrot groep">Mandelbrot</a> groep.</p>
                            <p>STRAK timmert reeds drie jaar aan de weg om haar klanten te helpen met <a href="/diensten" title="onze diensten">creatieve online- en offline communicatie</a>. Ze mocht in die jaren al heel wat mooie <a href="/cases" title="ontdek onze cases">cases</a> uitwerken voor KMO-klanten die de creatieve no-nonsense kijk op bedrijfscommunicatie wel weten te smaken.</p>
                            <p>Vaak werd voor deze projecten samengewerkt met twee andere bedrijven uit de groep namelijk <a href="http://www.publilab.be/" target="_blank" title="PubliLab">PubliLab</a> voor Online Marketing en <a href="http://www.talkingheads.be/nl/" title="Talking Heads">Talking Heads</a> voor Social Media. Ze deelden zelfs kantoorruimtes. Zo groeide stilaan het idee om verder samen te gaan als één team om nog vlotter en eenvoudiger te kunnen samenwerken, en dit onder de naam STRAK.</p>
                            <p>Christophe Legrand, Managing Partner aan het woord: ‘We merken dat klanten meer en meer vragen naar een <a href="/aanpak" title="Onze aanpak">globale aanpak</a> waarbij wij als partner instaan voor alle communicatie. Op dat vlak was het samenbrengen van de verschillende bedrijven dan ook een logische stap. We werken uiteraard nog steeds samen met andere bedrijven om onze klanten te helpen, maar het voelt goed om nu een globaal verhaal aan te bieden dat reikt van concept tot development, design en daarna de online marketing’</p>
                            <p>Jeff Van Loo, Creative Director van STRAK: ‘Ik voel een hele sterke dynamiek met het nieuwe team nu we onze eilandjes hebben verlaten. Persoonlijk sta ik te popelen om nieuwe projecten, zowel klein als groot, te begeleiden in hun zoektocht naar de juiste <a href="/diensten/branding" title="branding">creatieve insteek</a> om ze te laten opvallen tussen alle andere merken’.</p>
                            <p>STRAK biedt nu ook <a href="/diensten/marketing" title="online marketing">Online Marketing</a> aan waarbij Joeri De Bouvere (voorheen PubliLab) Head of Marketing wordt. ‘Ik doe al meer dan 10 jaar Online Marketing maar heb nooit echt de doorgedreven ondersteuning die STRAK met zich meebrengt ter beschikking gehad, dat zal nu wel het geval zijn en dat is een grote meerwaarde’.</p>
                            <p><strong>Of het nu over een identiteit, campagne of website gaat wij maken jouw merk zichtbaar, relevant en effectief. Digitaal, traditioneel of iets dat nog niet bestaat. Geen probleem. STRAK doet er alles aan om je merk of bedrijf de visibiliteit te geven die het verdient.</strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-12 col-md-4">
                            <ul>
                                <li>Christophe Legrand</li>
                                <li><a href="tel:+32479491538" title="telefoonnummer" class="u-txt-decoration--none">0479 49 15 38</a> </li>
                                <li><a href="mailto:christophe@strak.be" title="email">christophe@strak.be</a> </li>
                            </ul>
                        </div>
                        <div class="col col-12 col-md-4">
                            <ul>
                                <li>Jeff Van Loo</li>
                                <li><a href="tel:+32478207698" title="telefoonnummer" class="u-txt-decoration--none">0478 20 76 98</a> </li>
                                <li><a href="mailto:jeff@strak.be" title="email">jeff@strak.be</a> </li>
                            </ul>
                        </div>
                        <div class="col col-12 col-md-4">
                            <ul>
                                <li>Joeri De Bouvere</li>
                                <li><a href="tel:+32476905378" title="telefoonnummer" class="u-txt-decoration--none">0476 90 53 78</a> </li>
                                <li><a href="mailto:joeri@strak.be" title="email">joeri@strak.be</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
    </section>
        <?php include '../includes/cta.php' ?>
    </main>


<?php include '../includes/footer.php' ?>