<?php
$page = 'senior-developer';
include '../includes/header.php';
?>

<main class="c-site-content">
   <section class="o-section u-padding-top--s">
       <div class="container">
           <div class="row">
               <div class="col col-12 u-m-top--negative">
                   <h4>Jobs  -  Senior Php Developer met ervaring</h4>
                   <h1>Word jij onze nieuwe Senior PHP developer</h1>
                   <p>We zijn op zoek naar iemand die zijn of haar creativiteit en nieuwsgierigheid wil loslaten op diverse projecten en een goede aanvulling is voor ons huidig team. Iemand die van ons kan leren en ons iets kan bijleren.</p>
               </div>
           </div>
           <div class="row u-padding-top--xl">
               <div class="col col-12">
                   <h5>Je profiel</h5>
                   <p>Naast iemand die vlot is in omgang, open staat voor een grapje maar ook weet van een professionele aanpak zoeken we natuurlijk iemand met de nodige kennis. Je kan vlot overweg met PHP. Een goede basis doet al veel, we bieden ook de nodige opleiding om die kennis uit te breiden. Verdere kennis van Open Source Content Management Systemen waaronder Drupal of WordPress is zeker een plus. Basiskennis kan natuurlijk worden bijgeschaafd tot je een expert bent.</p>
               </div>
           </div>
           <div class="row">
               <div class="col col-12 col-lg-6 u-padding-top--xl">
                   <h5>Wat we verwachten</h5>
                   <ul class="c-list c-list--asterisk">
                       <li>Uitstekende kennis van PHP 5.6/7, MySQL.</li>
                       <li>Bijkomende kennis van HTML, CSS, JavaScript</li>
                       <li>Kennis MVC, OOP, namespaces, git.</li>
                       <li>Ervaring met WordPress.</li>
                       <li>Ervaring met Laravel is een plus.</li>
                   </ul>
               </div>
               <div class="col col-12 col-lg-5 offset-lg-1 u-padding-top--xl    ">
                   <h5>Wat we bieden</h5>
                   <ul class="c-list c-list--asterisk">
                       <li>Conform salaris.</li>
                       <li>Leuke omgeving.</li>
                       <li>Open bedrijfssfeer.</li>
                       <li>Uitdagende job in een dynamisch team.</li>
                       <li>Ruimte om je kennis te ontwikkelen.</li>
                   </ul>
               </div>
           </div>
           <p>Wij zien graag je CV met motivatie. Een link naar je portfolio of online projecten zijn ook welkom.</p>
           <div class="button-container-mobile"><a href='&#109;ai&#108;to&#58;&#37;6Aobs&#64;%&#55;3tr&#97;&#37;&#54;B%2E&#98;&#37;6&#53;?SUBJECT=Senior%20developer' class="c-btn c-btn--gradient">Interesse?</a></div>
       </div>
   </section>
</main>
    <?php include '../includes/footer.php'; ?>