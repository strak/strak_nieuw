<?php
$page = 'jobs';
include '../includes/header.php';
?>
<main class="c-site-content">
   <section class="o-section u-padding-top--s">
       <div class="container">
           <div class="row">
               <div class="col col-12 u-m-top--negative">
                   <h4>Jobs</h4>
                   <h1>Word jij onze nieuwe collega?</h1>
                   <p>Geef maar toe, je bent niet zomaar op deze pagina gekomen. Heb je zin om bij STRAK te komen werken en met jouw skills onze familie te versterken? Aha, dat klinkt als muziek in de oren. Misschien kan één van onderstaande vacatures jou wel bekoren.</p>
               </div>
           </div>
           <div class="row no-gutters c-cta--case" data-aos="fade-in">
               <div class="col col-12 col-md-6 c-cta__image">
                   <div class="c-background-image js-lazyload" data-src="/images/jobs/2.png"></div>
               </div>
               <div class="col col-12 col-md-6 c-cta__body" data-aos="fade-up" data-aos-delay="-200">
                   <h2><span class="d-block u-padding-left--s">Senior</span>
                           <span class="d-block">PHP developer</span>
                   </h2>
                   <a href="/jobs/senior-developer" class="c-btn c-btn--gradient" title="senior php developer">Da’s iets voor mij</a>
               </div>
           </div>
           <div class="row no-gutters c-cta--case" data-aos="fade-in">
               <div class="col col-12 col-md-6 c-cta__image">
                   <div class="c-background-image js-lazyload" data-src="/images/jobs/_DSC2645-min.png"></div>
               </div>
               <div class="col col-12 col-md-6 c-cta__body order-md-first" data-aos="fade-up" data-aos-easing="ease">
                   <h2><span class="d-block u-padding-left--s">Junior</span>
                       <span class="d-block">PHP developer</span>
                       <span class="d-block u-padding-left--xxl">met ervaring</span>
                   </h2>
                   <a href="/jobs/junior-developer" class="c-btn c-btn--gradient" title="junior php developer">Da’s iets voor mij</a>
               </div>
           </div>
           <div class="row no-gutters c-cta--case" data-aos="fade-in">
               <div class="col col-12 col-md-6 c-cta__image">
                   <div class="c-background-image js-lazyload" data-src="/images/jobs/2.png"></div>
               </div>
               <div class="col col-12 col-md-6 c-cta__body" data-aos="fade-up" data-aos-delay="-200">
                   <h2>
                       <span class="d-block">Junior</span>
                       <span class="d-block  u-padding-left--xl">online</span>
                       <span class="d-block ">marketeer</span>
                   </h2>
                   <a href="/jobs/junior-online-marketeer" class="c-btn c-btn--gradient" title="senior php developer">Da’s iets voor mij</a>
               </div>
           </div>
           <div class="row justify-content-center">
               <div class="col col-12 col-md-6 u-text--center">
                   <p>Pin je niet vast op bovenstaande vacatures.
                       Heb je iets te bieden? Kom dan zeker langs en verras ons met jouw talenten.</p>
                   <a href="/contact" class="c-btn c-btn--gradient" title="contact">Contacteer ons</a>
               </div>
           </div>
       </div>
   </section>
    <div class="c-job--internship">
        <div class="container">
            <div class="row">
                <div class="col col-12">
                    <p><strong>Op zoek naar een boeiende stageplaats waar je geen koffie moet zetten?</strong></p>
                    <p>Stuur dan jouw motivatie en een staaltje van jouw kunsten naar <a href='mai&#108;to&#58;&#115;ta%67e&#64;s%74&#114;a%6B%2&#69;&#98;e'>s&#116;&#97;ge&#64;strak&#46;b&#101;</a></p>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include '../includes/footer.php'; ?>