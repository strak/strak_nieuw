<?php
$page = 'junior-online-marketeer';
include '../includes/header.php';
?>

<main class="c-site-content">
   <section class="o-section u-padding-top--s">
       <div class="container">
           <div class="row">
               <div class="col col-12 u-m-top--negative">
                   <h4>Jobs  -  Junior online marketeer</h4>
                   <h1>Word jij onze nieuwe Junior online marketeer</h1>
               </div>
           </div>
           <div class="row u-padding-top--xl">
               <div class="col col-12">
                   <h5>Je profiel</h5>
                   <p>We zoeken een junior online marketeer die heel veel zin heeft om er in te vliegen! Iemand die zijn of haar nieuwsgierigheid wil loslaten op diverse, uitdagende projecten én een aanvullig is voor ons team. </p>
               </div>
           </div>
           <div class="row">
               <div class="col col-12 col-lg-6 u-padding-top--xl">
                   <h5>Wat we verwachten</h5>
                   <ul class="c-list c-list--asterisk">
                       <li>Je hebt gezond boerenverstand (eenvoudige dingen zijn beter dan complexe)</li>
                       <li>Kennis van Google Adwords, Google Analytics en Social Media (advertising)</li>
                       <li>Voorbereidend onderzoek kunnen doen op beperkte tijd</li>
                       <li>Leergierig zijn, nieuwe kansen zien</li>
                       <li>Naast een goed gevoel voor humor, heb je ook heel wat taalgevoel</li>
                   </ul>
               </div>
               <div class="col col-12 col-lg-5 offset-lg-1 u-padding-top--xl    ">
                   <h5>Wat we bieden</h5>
                   <ul class="c-list c-list--asterisk">
                       <li>Een job met variatie waar je je 100% kan focussen op online marketing</li>
                       <li>We geven ruimte voor (zelf)studie + werken aan het merk (Strak) </li>
                       <li>Een leuk kantoor, toffe sfeer en top werkmateriaal om mee te werken</li>
                       <li>Flexible werkuren, want de loodgieter moet je ook kunnen binnenlaten </li>
                       <li>O ja, we geven ook een loon, niet onbelangrijk.</li>
                   </ul>
               </div>
           </div>
           <p>Wij zien graag je CV met motivatie. Een link naar je portfolio of online projecten zijn ook welkom.</p>
           <div class="button-container-mobile"><a href='&#109;ai&#108;to&#58;&#37;6Aobs&#64;%&#55;3tr&#97;&#37;&#54;B%2E&#98;&#37;6&#53;?SUBJECT=Junior%20online%20marketeer' class="c-btn c-btn--gradient">Interesse?</a></div>
       </div>
   </section>
</main>
    <?php include '../includes/footer.php'; ?>