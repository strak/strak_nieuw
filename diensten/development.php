<?php $page = 'development' ;

include '../includes/header.php'; ?>

    <main class="c-site-content">
        <section class="o-section u-padding-top--s c-branding">
            <div class="container">
                <div class="row">
                    <div class="col col-12 u-m-top--negative">
                        <h4>Diensten  -  Ontwikkeling</h4>
                        <h1>De digitale wereld staat niet stil.</h1>
                        <p>Meer nog..  Ze evolueert nog elke dag. Met die reden rekeningen we op het gekende én betrouwbare content management systemen.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid u-padding-left--flush u-padding-right--flush">
                <div class="row u-content--middle">
                    <div class="col col-12 col-md-6">
                        <img src="/images/diensten/_DSC2746.min.jpg" alt="Ontwikkeling" class="img-fluid"/>
                    </div>
                    <div class="col col-12 col-md-4 col-xl-3 offset-md-1 c-strategy__quote">
                        <p>Hierdoor hoef je niet te investeren in een complex systeem, ben je altijd mee en blijven jij én je klant veilig. Mooi, toch?</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <p>
                            Bij STRAK maken we naast prachtige en heldere websites ook online omgevingen die handig in gebruik zijn, met structuur en werken! Of het nu gaat om een webshop, een campagnesite of webplatform. Wij maken alle online omgevingen op maat, jouw maat.
                        </p>
                        <p>
                            Bovendien waken we er steeds op dat jouw online omgeving steeds je merk en identiteit belichaamt. Onze developers werken telkens heel nauw samen met de ontwerpers. Want een gebruiksvriendelijke én aantrekkelijke website online plaatsen is één ding. Een website bouwen die van jouw bezoekers, converterende klanten maakt is nog een ander paar mouwen. Maar wees gerust, wij doen het allebei.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <?php include '../includes/cta.php'; ?>
    </main>

<?php include '../includes/footer.php'; ?>