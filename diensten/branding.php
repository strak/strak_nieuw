<?php $page = 'branding' ;

include '../includes/header.php'; ?>

<main class="c-site-content">
   <section class="o-section u-padding-top--s c-branding">
       <div class="container">
           <div class="row">
               <div class="col col-12 u-m-top--negative">
                   <h4>Diensten  -  Branding & content</h4>
                   <h1>We bouwen unieke ervaringen <br>Voor merken die een onderscheid willen maken.</h1>
                   <p>Wij vormen en activeren merken door middel van inzicht, visie en uitvoering. Aan de hand van strategische inzichten, ontwerpsystemen en originele inhoud cre&#235;ren we authentieke merkeigenschappen die bedrijven vooruit helpen.</p>
               </div>
           </div>
           <div class="row">
               <div class="col col-12">
                   <h3>Een goeie branding start altijd vanuit een sterk concept</h3>
                   <p>
                       Wij gaan altijd op zoek naar een onderliggend verhaal die betekenis geeft aan een bedrijf of merk. Door ons te baseren op dergelijke waarden en/of kenmerken kunnen we er van overtuigd zijn dat het gekozen concept z'n impact niet zal missen. Een sterk concept moet zowel inhoudellijk als visueel kloppen, de ideale oefening daarvoor is de creatie van een logo. Hieronder kan je aan de hand van enkele slides  zien hoe wij dit voor Sportwerk Vlaanderen aangepakt hebben.</p>
               </div>
           </div>
       </div>
       <div class="container">
           <div class="row">
               <div class="col col-12">
                   <p>Hoe gaan we van concept naar logo?</p>
               </div>
           </div>
       </div>
       <div class="container-fluid u-padding-right--flush u-padding-left--flush">

                   <div id="branding-slider" class="js-slider c-slider" data-fade="true">
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-01-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-02-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-03-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-04-min.jpg"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-05-min.jpg"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-06-min.jpg"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-07-min.jpg"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-08-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-09-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-10-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-11-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-12-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-13-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-14-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-15-min.jpg"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-16-min.png"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-17-min.jpg"></div>
                           </div>
                       </div>
                       <div class=" js-slider-item">
                           <div class="c-slide">
                               <div class="c-background-image js-lazyload" data-src="/images/branding/BRANDINGVOORBEELD-Sportwerk_web-18-min.jpg"></div>
                           </div>
                       </div>


                       <a class="hidden-sm-down c-slider__control c-slider__control--left js-slider-control d-none" href="#branding-slider" role="button" data-slide="prev">
                           <svg class="c-icon c-icon--left">
                               <use xlink:href="#left"/>
                           </svg>
                           <span class="u-sr-only"></span>
                       </a>
                       <a class="hidden-sm-down c-slider__control c-slider__control--right js-slider-control" href="#branding-slider" role="button" data-slide="next">
                           <span class="u-sr-only"></span>
                           <svg class="c-icon c-icon--right">
                               <use xlink:href="#right"/>
                           </svg>
                       </a>
                   </div>
       </div>
       <div class="container">
           <div class="row">
               <div class="col col-12">
                   <h3>En wordt in eerste plaats gedragen door een straf logo</h3>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Magazinemedia.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/namgrass.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/annicaert.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/binnenshuis.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/NXI.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/opsmuk.svg"></div>
                   </div>
               </div>
           </div>
           <div class="row u-padding-top--xxl">
               <div class="col col-12">
                   <h3>Maar stopt daar zeker niet</h3>
               </div>
           </div>
       </div>
       <div class="u-fullwidth--image">
          <img src="/images/branding/STRAK-branding-meer.jpg" alt="branding">
       </div>
       <div class="container">
           <div class="row u-padding-top--xxl">
               <div class="col col-12">
                   <p>Zie hier nog enkele andere logo's</p>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/sportwerk-vlaanderen.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Architektenburo.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/eKart.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/EFS.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/studioD.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Journalism-Tools.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Shifting-Gears.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/V-BOX.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Xamarin-Alliance.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/OAP.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/PAC.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/mark-makers.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/IDG.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Maatkasten.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Icarus.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Talking-Heads.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/JVL.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Mandelbrot.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/find.me.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/TECHORAMA.svg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/logos/Magnify.svg"></div>
                   </div>
               </div>
           </div>
       </div>
   </section>
  <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>