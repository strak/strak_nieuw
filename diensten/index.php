<?php
$page = 'diensten';
include '../includes/header.php';
?>
<main class="c-site-content">
   <section class="o-section u-padding-top--s">
       <div class="container">
           <div class="row">
               <div class="col col-12 u-m-top--negative">
                   <h4>Diensten</h4>
                   <h1>Digitaal, traditioneel of iets dat nog niet bestaat. Geen probleem.<br>
                       Wij denken en doen er alles aan om je merk of bedrijf de visibiliteit te
                       geven die het verdient.</h1>
               </div>
           </div>
           <div class="row content-middle c-card--services">
               <div class="col col-12 col-md-5 c-card__image" data-aos="fade-in">
                   <div class="c-background-image js-lazyload" data-src="/images/diensten/Strak_uitleg2.png"></div>
               </div>
               <div class="col col-12 col-md-6 offset-md-1" data-aos="fade-left">
                   <h2>Strategie & concept</h2>
                   <p>Geen helder concept zonder een doordachte strategie. Geen doordachte strategie zonder
                       een helder concept. Onze expertise stelt ons in staat om telkens naar creatieve en
                       intelligente oplossingen te zoeken om van jouw project een succesverhaal te maken. Samen
                       met onze klanten realiseren wij straffe belevenissen mét doordachte strategie en helder
                       concept.</p>
                   <a href="/diensten/strategie" title="Strategy en concept">Meer lezen</a>
               </div>
           </div>
           <div class="row content-middle c-card--services">
               <div class="col col-12 col-md-5 offset-md-1 c-card__image" data-aos="fade-in">
                   <div class="c-background-image js-lazyload" data-src="/images/diensten/_DSC2570.png"></div>
               </div>
               <div class="col col-12 col-md-6 order-md-first" data-aos="fade-right">
                   <h2>Branding & content</h2>
                   <p>Jouw merk of bedrijf versterken. Dit door een visuele en inhoudelijke make-over of een
                       gloednieuw idee. Door onze creativiteit en ervaring toe te leggen op jouw project, zorgen we
                       ervoor dat je bedrijf of merk staat als een huis, een prachtig huis. STRAK gaat telkens voor een
                       visuele creatie voor elke drager, zowel online als offline. </p>
                   <a href="/diensten/branding" title="Branding & concept">Meer lezen</a>
               </div>
           </div>
           <div class="row content-middle c-card--services">
               <div class="col col-12 col-md-5 c-card__image" data-aos="fade-in">
                   <div class="c-background-image js-lazyload" data-src="/images/diensten/_DSC2740.png"></div>
               </div>
               <div class="col col-12 col-md-6 offset-md-1" data-aos="fade-left">
                   <h2>Development</h2>
                   <p>Een bedrijfs- of campagne-website is cruciaal in de merkbeleving. We verzorgen het design
                       en de ontwikkeling op maat, jouw maat. We kiezen hier resoluut voor een Open Source
                       benadering. Profiteer zo van de expertise die de ondersteunende gemeenschap te bieden
                       heeft.</p>
                   <a href="/diensten/development" title="Development">Meer lezen</a>
               </div>
           </div>
           <div class="row content-middle c-card--services">
               <div class="col col-12 col-md-5 offset-md-1  c-card__image" data-aos="fade-in">
                   <div class="c-background-image js-lazyload" data-src="/images/diensten/klarafestival-webUIUX_cropped.png"></div>
               </div>
               <div class="col col-12 col-md-6 order-md-first" data-aos="fade-right">
                   <h2>Marketing & conversie</h2>
                   <p>Het uittekenen en uitzetten van de juiste marketingkoers voor jouw merk of bedrijf, is waar
                       we iedere dag voor gaan. Afhankelijk van je bedrijf en je doelstellingen, helpen we je de
                       juiste kanalen in te zetten en interessante bezoekers aan te trekken. Dit om een maximaal
                       resultaat te behalen. Daar waar nodig ondersteunen we bij de praktische uitvoering ervan en
                       wijzen we je de juiste weg.</p>
                   <a href="/diensten/marketing" title="Marketing en conversie">Meer lezen</a>
               </div>
           </div>
       </div>
   </section>
    <?php include '../includes/cta.php'; ?>
</main>
<?php include '../includes/footer.php'; ?>