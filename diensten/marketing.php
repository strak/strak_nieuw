<?php $page = 'marketing' ;

include '../includes/header.php'; ?>

<main class="c-site-content">
   <section class="o-section u-padding-top--s c-branding">
       <div class="container">
           <div class="row">
               <div class="col col-12 u-m-top--negative">
                   <h4>Diensten  -  Marketing & conversie</h4>
                   <h1>Wij brengen de juiste bezoekers naar jouw website, bezoekers die converteren en klant worden.</h1>
                   <p>Online marketing heeft als doel om meer klanten te bereiken te bereiken via de website of social mediakanalen.    Een mooie en functionele website is niet voldoende,  je moet er ook in slagen om de mensen naar je website te krijgen en er voor te zorgen dat ze ook converteren.</p>
               </div>
           </div>
           <div class="row">
               <div class="col col-12">
                   <h3>Wat kan je verwachten?</h3>
                   <p>
                       <h4>Meer bezoekers</h4>

                       Meer bezoekers bereiken is de eerste stap in online succes, dit kan via verschillende kanalen.
                       Wij begeleiden jou in het kiezen vanger die kanalen (meestal een mix) en het opstellen van een kalender met doelstellingen.
                   </p>
                   <p>
                       <h4>Meer leads</h4>
                       Meer bezoekers voor jouw website is de eerste stap, maar wij willen uiteraard wel de juiste bezoekers.
                       Daarom dat we in stap 1 al de basis leggen om hier effectief te kunnen werken naar meer leads door de keuze van de juiste doelgroepen.
                   </p>
                   <p>
                       <h4>Meer klanten</h4>
                       Leads binnenhalen en converteren naar klanten is de laatste stap en eentje waar we uiteraard ook op jou rekenen.
                       Maar op basis van data-analyse uit stap 1 en stap 2 kunnen we ook hier zoveel mogelijk bijsturen naar een beter resultaat.
                   </p>
               </div>
           </div>

           <div class="row">
               <div class="col col-12">
                   <h5>Een greep uit de klanten die we al hielpen met online marketing</h5>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/alphatent.png"></div>
                   </div>
               </div>

               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/belmodo.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/De-Juristen.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/freetime.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/Isotrie.JPG"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/kvik-logo.jpg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/logo nirwana.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/logo-nr4.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/pac-interiors-01.jpg"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/teakdeco-logo.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/vandenbussche-logo.png"></div>
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col col-12">
                   <h5>Welke netwerken zetten we daarvoor in?</h5>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/Google.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/Facebook.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/instagram.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/Linkedin logo.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/YouTube_logo_(2017).png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/mailchimp.png"></div>
                   </div>
               </div>
               <div class="col col-12 col-md-4">
                   <div class="c-logo">
                       <div class="c-background-image js-lazyload" data-src="/images/branding/marketing/bing logo.png"></div>
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col col-12">
                   <p>Graag bekijken we met jou welke kanalen we best inzetten (afhankelijk van doel en sector) en maken we zo een ideale mix. Uiteraard zorgen we ook voor de opvolging via Google Analytics en andere tools zodat je steeds beeld.</p>
               </div>
           </div>
       </div>
   </section>
  <?php include '../includes/cta.php'; ?>
</main>

<?php include '../includes/footer.php'; ?>