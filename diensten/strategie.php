<?php $page = 'strategy' ;

include '../includes/header.php'; ?>

    <main class="c-site-content">
        <section class="o-section u-padding-top--s c-branding">
            <div class="container">
                <div class="row">
                    <div class="col col-12 u-m-top--negative">
                        <h4>Diensten  -  Strategie en concept</h4>
                        <h1>De ervaring om te interpreteren, de vindingrijkheid om op te lossen, de creativiteit om te inspireren.</h1>
                        <p>Door een diepgaande inleving in de wereld van onze klanten zoeken we naar heldere inzichten en graven we naar de vraag achter de vraag. Elke geslaagde strategie of concept begint met het stellen van de juiste vragen binnen de juiste context in functie van de doelen die je wilt behalen. Begrijpen waarom je wilt doen wat je gaat doen, is daarom essentieel binnen elke vorm van communicatie.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid u-padding-left--flush u-padding-right--flush">
                <div class="row u-content--middle">
                    <div class="col col-12 col-md-6">
                        <img src="/images/diensten/_DSC2658.min.jpg" alt="Strategie en concept" class="img-fluid"/>
                    </div>
                    <div class="col col-12 col-md-4 col-xl-3 offset-md-1 c-strategy__quote">
                        <p>We streven telkens naar een resultaat dat naast intelligent ook effectief is, en 360° aansluit bij jouw doelpubliek.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <p>
                            Om dit te bereiken zal ons team aan experten jou eerst helpen om het grote geheel te zien, het grotere plaatje. Dit is noodzakelijk om jou op gepaste wijze de diensten en producten aan te bieden, die je zullen helpen jouw doelen te bereiken.
                        </p>
                        <p>
                            <strong>Hiervoor gaan we op zoek naar:</strong
                        <ul>
                            <li>De specifieke noden en behoeften van jou én van je doelgroep</li>
                            <li>Je (bedrijfs)doelstellingen, uitdagingen en opportuniteiten</li>
                            <li>De verschillende oplossingen voor jouw vraagstuk</li>
                            <li>Dé beste oplossing voor jouw bedrijf, merk of project</li>
                            <li>Het inregelen van onze juiste diensten en tools</li>
                        </ul>
                        </p>
                        <p>
                            Eens dit alles in kaart is gebracht, bedenken we een effectieve strategie die de leidraad zal zijn om jouw doelen te behalen en bovendien ook te verzilveren.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <?php include '../includes/cta.php'; ?>
    </main>

<?php include '../includes/footer.php'; ?>