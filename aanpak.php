<?php
$page = 'aanpak';

include 'includes/header.php';

?>
<main class="c-site-content">
   <section class="o-section u-padding-top--s">
       <div class="container">
           <div class="row">
               <div class="col col-12 u-m-top--negative">
                   <h4>Aanpak</h4>
                   <h1>Wij houden van onze job waarbij we de uitdagingen van onze klanten met veel goesting, toewijding & kennis van zaken helpen te realiseren</h1>
                   <p>STRAK is een team van gepassioneerde ontwerpers, marketeers en developers die creatief denken. Samen helpen we merken en bedrijven te innoveren en groeien. Dag na dag.</p>
               </div>
           </div>
           <div class="row u-padding-top--xl">
               <div class="col col-12 col-md-6">
                   <div class="button-container-mobile"><figure class="u-margin-bottom--flush">
                       <img src="/images/branding/aanpak/Strak_Icons_Tekengebied%201%20kopie%203.png" alt="een goed gesprek" class="img-fluid" width="150"/>
                       </figure></div>
                   <h3>1. Een goed gesprek</h3>
                   <p>Dat is wat er meestal maar nodig is om een samenwerking tot stand te brengen.
                   Tijdens onze kennismaking leren we jou en jouw bedrijf beter kennen. Onze expertise stelt ons
                   daarbij in staat om snel de juiste vragen te stellen en hiervoor concrete oplossingen aan
                       te bieden. Creativity as a Service, zo werken we samen naar een goede oplossing.</p>
               </div>
               <div class="col col-12 col-md-6">
                   <div class="button-container-mobile"><figure class="u-margin-bottom--flush">
                       <img src="/images/branding/aanpak/Strak_Icons_Tekengebied%201%20kopie%204.png" alt="een goed gesprek" class="img-fluid" width="150"/>
                       </figure></div>
                   <h3>2. Creatief concept</h3>
                   <p>Na het kennismakingsgesprek gaan we een stapje verder en zorgen we ervoor dat we
                       jouw vraag en zijn context duidelijk begrijpen. Dit alles zetten wij om in een eerste
                       creatief concept, dat volledig in lijn ligt met het verhaal van jouw bedrijf en alle verkregen
                       informatie.</p>
               </div>
               <div class="col col-12 col-md-6">
                   <div class="button-container-mobile"><figure class="u-margin-bottom--flush">
                       <img src="/images/branding/aanpak/Strak_Icons_Tekengebied%201.png" alt="een goed gesprek" class="img-fluid" width="150"/>
                       </figure></div>
                   <h3>3. 360° roll out</h3>
                   <p>Maar... Hier stopt het niet voor ons, wij houden nu eenmaal niet van vluggertjes. Wij
                       gaan voor een volledige roll-out van het concept. Ons doel is een volledige fit te
                       realiseren van a tot z met als maximaal eindresultaat ..</p>
               </div>
               <div class="col col-12 col-md-6">
                   <div class="button-container-mobile"><figure class="u-margin-bottom--flush">
                       <img src="/images/branding/aanpak/Strak_Icons_Tekengebied%201%20kopie%202.png" alt="een goed gesprek" class="img-fluid" width="150"/>
                       </figure></div>
                   <h3>4. Een glimlach</h3>
                   <p>Een tevreden klant. Word jij onze volgende tevreden klant?<br>
                       <a href="/cases/" title="cases">Zij gingen je al voor </a> </p>
               </div>
           </div>

           <div class="row no-gutters c-cta--case" data-aos="fade-in">
               <div class="col col-12 col-md-6 c-cta__image">
                   <div class="c-background-image js-lazyload" data-src="/images/cases/sportwerk.jpg"></div>
               </div>
               <div class="col col-12 col-md-6 c-cta__body" data-aos="fade-up">
                   <h2><span class="d-block u-padding-left--s">Alles valt of </span>
                       <span class="d-block u-padding-left--l">staat met hoe </span>
                           <span class="d-block">sterk het </span>
                           <span class="d-block u-padding-left--xl">concept is</span>
                   </h2>
                   <a href="/cases/sportwerk" class="c-btn c-btn--gradient" title="sportwerk" data-aos="fade-up">Case: Sportwerk</a>
               </div>
           </div>
       </div>
   </section>
    <?php include 'includes/cta.php' ?>
</main>

<?php include 'includes/footer.php'?>