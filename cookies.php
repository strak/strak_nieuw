<?php
$page = 'privacy';

include 'includes/header.php';
?>
<main class="c-site-content">
    <div class="o-section  u-padding-top--s">
        <div class="container">
            <div class="row">
                <div class="col col-12 u-m-top--negative">
                    <strong>Cookies, je hebt ze in alle kleuren en smaken, maar ook op onze website.</strong>
                    <br/>
                    Jup, we steken het niet onder stoelen noch onder banken, ook op <a href="http://www.strak.be" title="strak.be">STRAK.be</a> maken we gebruik van cookies. En dit met een hele goede reden! Bij <a href="http://www.strak.be" title="strak.be">STRAK.be</a> hanteren we cookies om jou als bezoeker te identificeren. Dit om je zo specifieke diensten aan te bieden en je zonder problemen te laten surfen op onze website.<br/><br/>

                    Maar wat voor “dingen” zijn dat nu eigenlijk die cookies? Wel, heel simpel! Cookies zijn kleine stukjes informatie die in de vorm van tekstbestanden worden aangemaakt door <a href="http://www.strak.be" title="strak.be">STRAK.be</a> of een partner integratie. Vervolgens worden deze cookies op je harde schijf geplaatst wanneer je de STRAK website bezoekt.<br/><br/>

                    Door gebruik te maken van cookies op onze website wordt vermeden dat je je niet voortdurend kenbaar moet maken. Bijvoorbeeld voor gepersonaliseerde diensten, zoals het invullen van je gebruikersnaam of het kiezen van een taal of voor gerichte advertenties.<br/><br/>

                    Met de hand op ons hart, cookies worden niet op onze website geplaatst om je te bespieden of te achtervolgen. Wij bij STRAK, en de diensten waar we gebruik van maken, slaan geen persoonlijke gegevens op die direct gekoppeld zijn aan jou. De informatie die de cookies verzamelen, gebruiken we enkel en alleen maar om jouw bezoek aan onze website zo aangenaam mogelijk te houden en onze website verder te optimaliseren.<br/><br/>

                    <strong>Wil je optimaal genieten en rondsurfen op onze website?</strong><br/>

                    Aha, dan raden we je aan je cookies ingeschakeld te houden! Uiteraard ben je oud en wijs genoeg om te doen én te laten wat je zelf wilt. Jij beslist zelf om je cookies uit te schakelen op onze website.<br/><br/>

                    Op strak.be maken we gebruik van de volgende cookies:<br/>

                    <table>
                        <thead>
                        <td width="15%">
                            cookie
                        </td>
                        <td width="15%">
                            origine
                        </td>
                        <td width="55%">
                            doel
                        </td>
                        <td width="15%">
                            duurtijd
                        </td>
                        </thead>
                        <tr>
                            <td>
                                __gat
                            </td>
                            <td>
                                Google Analytics
                            </td>
                            <td>
                                Gebruikt door Google om het aantal verbindingen te beperken
                            </td>
                            <td>
                                Sessie
                            </td>
                        </tr>
                        <tr>
                            <td>
                                __gid
                            </td>
                            <td>
                                Google Analytics
                            </td>
                            <td>
                                Registreert een uniek identificatie nummer gebruikt om statistische data te genereren over het gebruik van de website.
                            </td>
                            <td>
                                Sessie
                            </td>
                        </tr>
                        <tr>
                            <td>
                                _hj
                            </td>
                            <td>
                                Hotjar
                            </td>
                            <td>
                                Registreert een uniek identificatie nummer gebruikt om statistische data te genereren over het gebruik van de website.
                            </td>
                            <td>
                                1 Jaar
                            </td>
                        </tr>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php
    include 'includes/cta.php';
    ?>
</main>

<?php
include 'includes/footer.php';
?>
