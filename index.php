<?php $page = 'index';
?>
<?php include 'includes/header.php'?>
<main class="c-site-content">
    <section class="o-section">
        <div class="container">
            <div class="row content-middle c-card--home ">
                <div class="col col-12 col-md-7  c-card__image" data-aos="fade-in">
                    <div class="c-background-image js-lazyload" data-src="/images/homepage/_DSC2675-min.png"></div>
                </div>
                <div class="col col-12 col-md-4 offset-lg-1 u-text--right  c-card__body" data-aos="fade-right">
                    <h3>Wij houden van onze job waarbij we de uitdagingen van onze klanten met veel goesting, toewijding & kennis van zaken helpen te realiseren.</h3>
                    <div class="button-container-mobile"><a href="/aanpak" title="aanpak" class="c-btn c-btn--gradient">Onze aanpak</a></div>
                </div>
            </div>
            <div class="row content-middle c-card--home">
                <div class="col col-12 col-lg-7 col-md-6 offset-md-1 c-card__image" data-aos="fade-in">
                    <div class="c-background-image js-lazyload" data-src="/images/homepage/_DSC7013-min.png"></div>
                </div>
                <div class="col col-12 col-md-5  col-lg-4 u-text--left order-md-first c-card__body" data-aos="fade-left">
                    <h3>Digitaal, traditioneel of iets dat nog niet bestaat. Wij denken en doen er alles aan om je merk of bedrijf de visibiliteit te geven die het verdient.</h3>
                    <div class="button-container-mobile"><a href="/diensten" title="aanpak" class="c-btn c-btn--gradient">Onze diensten</a></div>
                </div>
            </div>
            <div class="row content-middle c-card--home">
                <div class="col col-12 col-md-7 c-card__image" data-aos="fade-in">
                    <div class="c-background-image js-lazyload" data-src="/images/homepage/_DSC8842-min.png"></div>
                </div>
                <div class="col col-12 col-md-4 offset-lg-1 u-text--right c-card__body" data-aos="fade-left">
                    <h3>Onze honger en ons vakmanschap drijft ons om telkens zorgvuldig doordachte concepten te ontwikkelen die helder, flexibel en schaalbaar zijn.</h3>
                    <div class="button-container-mobile"><a href="/cases/" title="aanpak" class="c-btn c-btn--gradient">Cases</a></div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'includes/cta.php'?>
</main>

<?php include 'includes/footer.php'?>
