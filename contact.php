<?php

function clean_string($string) {
    $bad = array("content-type","bcc:","to:","cc:","href");
    return str_replace($bad,"",$string);
}

$errors = [];

if(isset($_POST['email']) && isset($_POST['url']) && $_POST['url'] == '') {

    if(!isset($_POST['name'])) {
        $errors['name'] = "Het veld /'Naam/' is verplicht.<br/>";
    }

    if(!isset($_POST['telephone'])) {
        $errors['telephone'] = "Het veld /'Telefoon/' is verplicht.<br/>";
    }

    if(!isset($_POST['email'])) {
        $errors['email'] = "Het veld /'Email/' is verplicht.<br/>";
    }

    if(!isset($_POST['message'])) {
        $errors['message'] = "Het veld /'Bericht/' is verplicht.<br/>";
    }

    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email_format'] = "Uw email adres blijkt niet correct te zijn.<br/>";
    }

    if(count($errors) == 0) {

        $email_to = "plan@strak.be";
        $email_subject = "Email via strak.be";

        $email_message = "";
        $email_from = clean_string($_POST['email']);

        $email_message .= "Name: ".clean_string($_POST['name'])."\n";
        $email_message .= "Email: ".$email_from."\n";
        $email_message .= "Telefoon: ".clean_string($_POST['telephone'])."\n";
        $email_message .= "Bericht: ".clean_string($_POST['message'])."\n";
        $email_message .= "Opt in: ". isset($_POST['optin']) ."\n";

        $headers = 'From: ' . $email_from . "\r\n" .
                   'Reply-To: ' . $email_from . "\r\n" .
                   'X-Mailer: PHP/' . phpversion() . "\r\n";

        if(mail($email_to, $email_subject, $email_message, $headers)) {

            header('location: /bedankt');

        }else{
            $errors['mailerror'] = "Er is iets misgelopen bij het versturen van uw mail. Wij zijn er van op de hoogte gebracht. Probeer het later even opnieuw.<br/>";
        }
    }else {

    }

}

$page = 'contact';
include 'includes/header.php';
?>
    <main class="c-site-content">
        <section class="o-section u-padding-top--s">
            <div class="container">
                <div class="row">
                    <div class="col col-12 col-md-12 u-m-top--negative u-margin-bottom--xl">
                        <h4>Contact</h4>
                        <h1>Zin om een partijtje creatief te sparren?<br>Aarzel dan zeker niet om ons te porren, we zijn benieuwd naar jouw project of vragen.</h1>
                    </div>
                    <div class="col col-12 col-md-4">
                        <div class="row">
                            <div class="col col-12 u-margin-bottom--xl">
                                <img src="images/STRAK-kantoor.jpg" alt="kantoor" class="img-fluid"/>
                            </div>
                            <ul class="col col-12 c-list c-list--contact">
                                <li><strong>STRAK PLAN bvba</strong></li>
                                <li>Evergemsesteenweg 195</li>
                                <li>B-9032 Gent</li>
                                <li><a href="tel:+32-9-335-22-74" title="telefoonnummer">+32  335 22 74</a> </li>
                                <li><a href='mailto&#58;pla%6&#69;%40st&#114;ak&#46;%&#54;2e' title="mail ons">plan&#64;strak&#46;b&#101;</a></li>
                                <li>BE 0627 793 007</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col col-12 col-md-7 offset-md-1">
                        <p class="contact-text">
                            Ben jij klaar om je organisatie of merk toekomstgericht te maken? Of heb je gewoon een vraag over één van onze diensten? Van branding tot alle online activiteiten: wij zijn je partner! Contacteer ons voor een vrijblijvend gesprek.
                        </p>
                        <p>
                            Laat ons iets weten, de koffie staat al klaar!
                        </p>
                        <form method="post" class="c-form--contact u-padding-top--xl" id="contact-form" action="">
                            <?php
                            if(isset($errors['mailerror'])){
                                echo '<div class="alert alert-danger" role="alert">'.$errors['mailerror'].'</div>';
                            }
                            if(isset($errors['name'])){
                                echo '<div class="alert alert-danger" role="alert">'.$errors['name'].'</div>';
                            }
                            ?>
                            <input type="text" placeholder="naam*" name="name" required value="<?php echo (isset($_POST['name']))? $_POST['name'] : ""; ?>">
                            <?php
                            if(isset($errors['telephone'])){
                                echo '<div class="alert alert-danger" role="alert">'.$errors['telephone'].'</div>';
                            }
                            ?>
                            <input type="text" placeholder="telefoon*" name="telephone" required value="<?php echo (isset($_POST['telephone']))? $_POST['telephone'] : ""; ?>">
                            <?php
                            if(isset($errors['email'])){
                                echo '<div class="alert alert-danger" role="alert">'.$errors['email'].'</div>';
                            }
                            if(isset($errors['email_format'])){
                                echo '<div class="alert alert-danger" role="alert">'.$errors['email_format'].'</div>';
                            }
                            ?>
                            <input type="email" placeholder="e-mail*" name="email" required value="<?php echo (isset($_POST['email']))? $_POST['email'] : ""; ?>">
                            <p class="antispam">Leave this empty: <input type="text" name="url" /></p>
                            <?php
                            if(isset($errors['message'])){
                                echo '<div class="alert alert-danger" role="alert">'.$errors['message'].'</div>';
                            }
                            ?>
                            <textarea placeholder="bericht*" name="message" required><?php echo (isset($_POST['message']))? $_POST['message'] : ""; ?></textarea>
                            <small><i>Velden met een <sup>*</sup> zijn verplicht</i></small><br/>
                            <input id="optin" type="checkbox" name="optin" value="1" <?php echo(isset($_POST['optin']))? 'checked' : '' ;?>/><label for="optin">Ik geef toestemming om verdere communicatie van STRAK te ontvangen.</label>
                            <div class="button-container-mobile"><input type="submit" value="Ik wil koffie" class="c-btn c-btn--gradient"></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'includes/cta2.php'; ?>
    </main>
<?php if(count($errors) != 0) : ?>
    <script>
        var elmnt = document.getElementById("contact-form");
        elmnt.scrollIntoView();
    </script>
<?php endif; ?>
<?php include 'includes/footer.php'; ?>