<?php
$page = 'disclaimer';

include 'includes/header.php';
?>
<main class="c-site-content">
    <div class="o-section  u-padding-top--s">
        <div class="container">
            <div class="row">
                <div class="col col-12 u-m-top--negative">
                    <p>
                        Deze website is eigendom van STRAK Plan BVBA.
                    </p>
                    <p>
                        Contactgegevens en Maatschappelijk zetel:
                        STRAK Plan BVBA
                        Voorhavenlaan 31/003
                        9000 Gent
                    </p>
                    <p>
                        Telefoon: 09 335 22 80
                        Email: plan@strak.be
                    </p>
                    <p>
                        Ondernemingsnummer: BTW BE 0627 793 007
                    </p>
                    <p>
                        Door de toegang tot en het gebruik van de website verklaart u zich uitdrukkelijk akkoord met de volgende algemene voorwaarden.
                    </p>
                    <p>
                        Intellectuele eigendomsrechten
                        De inhoud van deze site, met inbegrip van de merken, logo’s, tekeningen, data, product- of bedrijfsnamen, teksten, beelden e.d. zijn beschermd door intellectuele rechten en behoren toe aan STRAK of rechthoudende derden.
                    </p>
                    <p>
                        Beperking van aansprakelijkheid
                        De informatie op de website is van algemene aard. De informatie is niet aangepast aan persoonlijke of specifieke omstandigheden, en kan dus niet als een persoonlijk, professioneel of juridisch advies aan de gebruiker worden beschouwd.
                    </p>
                    <p>
                        STRAK levert grote inspanningen opdat de ter beschikking gestelde informatie volledig, juist, nauwkeurig en bijgewerkt zou zijn. Ondanks deze inspanningen kunnen onjuistheden zich voordoen in de ter beschikking gestelde informatie. Indien de verstrekte informatie onjuistheden zou bevatten of indien bepaalde informatie op of via de site onbeschikbaar zou zijn, zal STRAK de grootst mogelijke inspanning leveren om dit zo snel mogelijk recht te zetten.
                    </p>
                    <p>
                        STRAK kan evenwel niet aansprakelijk worden gesteld voor rechtstreekse of onrechtstreekse schade die ontstaat uit het gebruik van de informatie op deze site.
                    </p>
                    <p>
                        Indien u onjuistheden zou vaststellen in de informatie die via de site ter beschikking wordt gesteld, kan u de beheerder van de site contacteren.
                    </p>
                    <p>
                        De inhoud van de site (links inbegrepen) kan te allen tijde zonder aankondiging of kennisgeving aangepast, gewijzigd of aangevuld worden. STRAK geeft geen garanties voor de goede werking van de website en kan op geen enkele wijze aansprakelijk gehouden worden voor een slechte werking of tijdelijke (on)beschikbaarheid van de website of voor enige vorm van schade, rechtstreekse of onrechtstreekse, die zou voortvloeien uit de toegang tot of het gebruik van de website.
                    </p>
                    <p>
                        STRAK kan in geen geval tegenover wie dan ook, op directe of indirecte, bijzondere of andere wijze aansprakelijk worden gesteld voor schade te wijten aan het gebruik van deze site of van een andere, inzonderheid als gevolg van links of hyperlinks, met inbegrip, zonder beperking, van alle verliezen, werkonderbrekingen, beschadiging van programma's of andere gegevens op het computersysteem, van apparatuur, programmatuur of andere van de gebruiker.
                    </p>
                    <p>
                        De website kan hyperlinks bevatten naar websites of pagina's van derden, of daar onrechtstreeks naar verwijzen. Het plaatsen van links naar deze websites of pagina’s impliceert op geen enkele wijze een impliciete goedkeuring van de inhoud ervan.
                    </p>
                    <p>
                        STRAK verklaart uitdrukkelijk dat zij geen zeggenschap heeft over de inhoud of over andere kenmerken van deze websites en kan in geen geval aansprakelijk gehouden worden voor de inhoud of de kenmerken ervan of voor enige andere vorm van schade door het gebruik ervan.
                    </p>
                    <p>
                        Toepasselijk recht en bevoegde rechtbanken.
                        Het Belgisch recht is van toepassing op deze site. In geval van een geschil zijn enkel de rechtbanken van het arrondissement R.P.R. Gent bevoegd.
                    </p>
                    <p>
                        Privacybeleid
                        STRAK hecht belang aan uw privacy. Hoewel de meeste informatie op deze site beschikbaar is zonder dat er persoonlijke gegevens moeten worden verstrekt, is het mogelijk dat de gebruiker om persoonlijke informatie gevraagd wordt. Deze informatie zal enkel gebruikt worden in het kader van ons klantenbeheer. De gebruiker kan zich, kosteloos en op verzoek, steeds verzetten tegen het gebruik van zijn gegevens voor direct marketing. Daartoe zal hij zich richten tot STRAK, Voorhavenlaan 31/3, 9000 Gent of via plan@strak.be. Uw persoonsgegevens worden nooit doorgegeven aan derden (indien wel het geval vermelden aan wie).
                    </p>
                    <p>
                        Conform de wet verwerking persoonsgegevens van 08/12/1992 beschikt de gebruiker over een wettelijk recht op inzage en eventuele correctie van zijn persoonsgegevens. Mits bewijs van identiteit (kopie identiteitskaart) kunt u via een schriftelijke, gedateerde en ondertekende aanvraag aan STRAK, Voorhavenlaan 31/3, 9000 Gent of via plan@strak.be, gratis de schriftelijke mededeling bekomen van uw persoonsgegevens. Indien nodig kunt u ook vragen de gegevens te corrigeren die onjuist, niet volledig of niet pertinent zouden zijn.
                    </p>
                    <p>
                        STRAK kan anonieme of geaggregeerde gegevens verzamelen van niet-persoonlijke aard, zoals browser type of IP-adres, het besturingsprogramma dat u gebruikt of de domeinnaam van de website langs waar u naar onze-website gekomen bent, of waarlangs u die verlaat. Dit maakt het ons mogelijk om onze website permanent te optimaliseren voor de gebruikers.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/cta.php';  ?>
</main>

<?php
include 'includes/footer.php';
?>
