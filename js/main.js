window.strak = window.strak || {};

strak.init = function () {

    strak.toggleNavigation();
    strak.blazy();
    strak.calcFooter();
    AOS.init( {
        disable: 'mobile',
        once: 'true'
    }
    );
    strak.slick();

    $( window ).resize(function() {
        strak.calcFooter();
    });

};

strak.toggleNavigation = function () {
    /**
     * Prevent body scroll on navigation toggled
     */
    $('.js-navigation-toggle').change(function(e) {
        var checked = $(this).is(':checked');

        if(checked) {
            $('body').addClass('no-scroll');
        } else {
            $('body').removeClass('no-scroll');
        }
    });
};
strak.blazy = function () {
    /**
     * Include and initialize blazy.js
     */
    var $blazy = new Blazy({
        selector: '.js-lazyload',
        successClass: 'is-loaded',
        offset: 100,
        error: function(el, msg) {
            if(msg === "missing") {
                console.error("bLazy: image is missing");
            }
            if(msg === "invalid") {
                console.error("bLazy: value is invalid");
            }
        }
    });

    $('.js-slider').on('afterChange', $blazy.revalidate);
};

strak.calcFooter = function () {

    var $footerPrimary = $('.c-site-footer--primary');
    var $footerSecondary = $('.c-site-footer--secondary');

    var $footerSpacer = $('.footer__spacer');
    var $footerContent = $('.c-footer__content');
    var $footer = $('.c-site-footer');

    var $height = $footerPrimary.outerHeight() + $footerSecondary.outerHeight();

    $footerSpacer.css("height", $height);
    $footerContent.css("height", $height);
    $footer.css("height", $height);

};

strak.slick = function () {
    $('.js-slider').slick({
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite:false,
        arrows: false,
        centerMode: true,
        centerPadding: '130px',
        initialSlide: 0,
        dots: true,
        fade: $(this).data('fade'),
        slide: '.js-slider-item',
        slidesToShow: $(this).data('slides-to-show'),
        slidesToScroll: $(this).data('slides-to-scroll'),
        speed: 500,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '80px',
                    slidesToShow: $(this).data('slides-to-show'),
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: $(this).data('slides-to-show'),
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: $(this).data('slides-to-show'),
                }
            }
        ]
    });

    $('.js-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
        if(currentSlide === 0) {
            $('.c-slider__control--left').addClass('d-none');
            $('.c-slider__control--right').removeClass('d-none');
        } else if (currentSlide === 17) {
            $('.c-slider__control--right').addClass('d-none');
            $('.c-slider__control--left').removeClass('d-none');
        } else {
            $('.c-slider__control--left').removeClass('d-none');
            $('.c-slider__control--right').removeClass('d-none');

        }

       console.log(currentSlide);
    });

};

jQuery( document ).ready(function() {
// initialize on docready
    jQuery(strak.init);
});
