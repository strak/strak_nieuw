(function ($) {

	$(document).ready(function() {

		/**
		 * Slider controls
		 */
		$('.js-slider-control').on('click', function(e) {
			switch($(this).data('slide')) {
				case 'prev':
					$($(this).attr('href')).slick('slickPrev');
					break;
				case 'next':
					$($(this).attr('href')).slick('slickNext');
					break;
				default:
					var index = $($(this).attr('href')).find('#' + $(this).data('slide')).data('slick-index');
					$($(this).attr('href')).slick('goTo', index);
					break;
			}

			e.preventDefault();
		});

		/**
		 * Slider indicators
		 */
		$('.js-slider-indicator').on('click', function(e) {
			$($(this).attr('href')).slick('slickGoTo', $(this).data('slide-to'));
			$($(this).attr('href')).find('.js-slider-indicator').removeClass('active');
			$(this).addClass('active');

			e.preventDefault();
		});

		$('.js-slider').on('beforeChange', function(e, slick, curretnSlide, nextSlide) {
			$(this).find('.js-slider-indicator').removeClass('active');
			$(this).find('.js-slider-indicator[data-slide-to="' + nextSlide + '"]').addClass('active');
		});

	});

})(jQuery);
